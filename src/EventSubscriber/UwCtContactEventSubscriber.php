<?php

namespace Drupal\uw_ct_contact\EventSubscriber;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\core_event_dispatcher\EntityHookEvents;
use Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent;
use Drupal\node\Entity\Node;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Contact event subscriber.
 *
 * @package Drupal\uw_ct_contact\EventSubscriber
 */
class UwCtContactEventSubscriber implements EventSubscriberInterface {

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor for event subscriber class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      EntityHookEvents::ENTITY_UPDATE => 'entityUpdate',
    ];
  }

  /**
   * Entity update hook.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent $event
   *   Entity update event.
   */
  public function entityUpdate(EntityUpdateEvent $event) {

    /**
     * Potentially contact entity.
     * @var \Drupal\node\Entity\Node $contact
     */
    $contact = $event->getEntity();

    // Trigger only for contact content type update.
    if (
      $contact &&
      $contact->bundle() === 'uw_ct_contact'
    ) {

      // Ensure that we have at least something for the old
      // and new media ids.
      $old_mid = NULL;
      $new_mid = NULL;

      // If there is an old listing image get the media id.
      if ($old = $contact->original->get('field_uw_contact_listing_image')->getValue()) {
        $old_mid = $old[0]['target_id'];
      }

      // If there is a new listing image, get the media id.
      if ($new = $contact->get('field_uw_contact_listing_image')->getValue()) {
        $new_mid = $new[0]['target_id'];
      }

      // If the old and new are not the same, then we have a change.
      if ($new_mid !== $old_mid) {
        $this->updateProfileListingImage($contact->id(), $old_mid, $new_mid);
      }

      // Update the profile fields.
      $this->updateProfileFields($contact);

      // Invalidate OFIS cache if Contact is unpublished
      // and there is watiam value provided.
      if (!$contact->isPublished() && $watiam = $contact->field_uw_ct_contact_watiam_id->value) {
        Cache::invalidateTags(['uw_wcms_ofis:user:' . $watiam]);
      }
    }
  }

  /**
   * Function to update the fields of profiles.
   *
   * @param \Drupal\node\Entity\Node $contact
   *   The contact node.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateProfileFields(Node $contact): void {

    // Get the fields to update from profiles module.
    $fields = _uw_ct_profile_alter_fields();

    // Get the nodes that have the contact sync.
    $nodes = $this->entityTypeManager
      ->getStorage('node')
      ->loadByProperties([
        'type' => 'uw_ct_profile',
        'field_uw_ct_profile_contact' => $contact->id(),
      ]);

    // Step through each node and fields and sync them.
    foreach ($nodes as $node) {
      foreach ($fields as $profile_field => $contact_field) {
        $node->$profile_field = $contact->$contact_field;
      }

      $node->save();
    }
  }

  /**
   * Function to change the listing image on profiles.
   *
   * @param int $nid
   *   The node id of the contact.
   * @param int $old_mid
   *   The old media id of the listing image.
   * @param int $new_mid
   *   The new media id of the listing image.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateProfileListingImage(int $nid, int $old_mid = NULL, int $new_mid = NULL): void {

    // Ensure that we get the right properties to load the nodes.
    // We need to remove the image if there is no image to look
    // for, since trying load with NULL for the listing image,
    // cause the site to error out.
    if (!$old_mid) {
      $properties = [
        'type' => 'uw_ct_profile',
        'field_uw_ct_profile_contact' => $nid,
      ];
    }
    else {
      $properties = [
        'type' => 'uw_ct_profile',
        'field_uw_ct_profile_contact' => $nid,
        'field_uw_ct_profile_image' => $old_mid,
      ];
    }

    // Get the nodes that have the contact sync and the same listing image.
    $nodes = $this->entityTypeManager
      ->getStorage('node')
      ->loadByProperties($properties);

    // Step through all the profile nodes and update the
    // listing image.
    foreach ($nodes as $node) {
      if ($node->field_uw_ct_profile_image->target_id) {
        $node->field_uw_ct_profile_image->target_id = $new_mid;
        $node->save();
      }
    }
  }

}
